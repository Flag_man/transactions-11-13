import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;

public class Bank {
    private HashMap<String, Account> accounts;
    private Random random = new Random();
    private final static long fraudAmount = 50000;
    private final int accountsNumber;

    public int getAccountsNumber() {
        return accountsNumber;
    }

    public Bank(int accountsNumber) {
        accounts = new HashMap<>();
        this.accountsNumber = accountsNumber;
        String newAccountNum;
        for (int i = 0; i < accountsNumber; i++) {
            newAccountNum = newAccountNum();
            accounts.put(newAccountNum, new Account(newAccountNum, (long) (Math.random() * 100000))); //Раздадим всем сестрам по серьгам, то бишь наполним банк баблищем щедрою рукой :)
        }
    }

    public synchronized HashMap<String, Account> getAccounts() {
        return accounts;
    }

    public synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)
            throws InterruptedException {
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами.
     * Если сумма транзакции > 50000, то после совершения транзакции,
     * она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка
     * счетов (как – на ваше усмотрение)
     */

    public void transfer(String fromAccountNum, String toAccountNum, long amount) throws InterruptedException {
        Account fromAccount = accounts.get(fromAccountNum);
        Account toAccount = accounts.get(toAccountNum);
        synchronized (fromAccountNum.compareTo(toAccountNum) < 0 ? fromAccount : toAccount) { //С тернарником действительно краше :):):)
            synchronized (fromAccountNum.compareTo(toAccountNum) < 0 ? toAccount : fromAccount) {
                if (fromAccount.getMoney() >= amount && fromAccount.getActiveStatus() && toAccount.getActiveStatus()) {
                    if (amount > fraudAmount) { //Если сумма большая, то делаем проверку СБ
                        if (isFraud(fromAccountNum, toAccountNum, amount)) { //Если проверка завалена, блочим счета
                            fromAccount.setActiveStatus(false);
                            toAccount.setActiveStatus(false);
                            return; // и с return-ом :):):)
                        }
                    }
                    fromAccount.setMoney(fromAccount.getMoney() - amount);
                    toAccount.setMoney(toAccount.getMoney() + amount);
                }
            }
        }
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    public synchronized long getBalance(String accountNum) {
        return accounts.get(accountNum).getMoney();
    }

    // Метод генерации нового счета. Конечно, лажа полная, на самом деле номера счетов явно генерируются по порядку, но нас в данном случае это волнует чуть менее, чем вообще никак :)
    public String newAccountNum() {
        String accNumber;
        do {
            accNumber = Double.toString(Math.random()).replace("0.", "");
        } while (accNumber.length() != 16);
        return accNumber;
    }

    //Метод, возвращающий общую сумму денег в банке
    public synchronized long getAllBalance() {
        return accounts.values().stream().mapToLong(Account::getMoney).sum();
    }

    //Метод, печатающий общую сумму денег в банке
    public void printAllBalanceString() {
        System.out.println("Общая сумма в банке - " + new DecimalFormat("###,###,###,###.## рублей").format(getAllBalance()));
    }

}
