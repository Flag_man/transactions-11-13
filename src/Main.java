import java.text.DecimalFormat;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank(10);
        long oldBalance = bank.getAccounts().values().stream().mapToLong(Account::getMoney).sum();

        int threadCount = 10;
        TransferThread[] transferThreads = new TransferThread[threadCount];
        Thread[] threads = new Thread[threadCount];

        for (int i = 0; i < threadCount; i++) {
            transferThreads[i] = new TransferThread(bank);
            threads[i] = new Thread(transferThreads[i]);
            threads[i].start();
        }

        //Ждем окончания работы всех потоков переводов средств
        while (!areAllThreadsComplete(threads)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
        System.out.println("Состояния счетов до попытки перевода");
        System.out.println("Общая сумма в банке - " + new DecimalFormat("###,###,###,###.## рублей").format(oldBalance));
        System.out.println();
        System.out.println("Состояния счетов после попытки перевода");
        bank.printAllBalanceString();
        System.out.println();
        bank.getAccounts().values().forEach(System.out::println);
    }

    //Замутим проверку - закончились ли наши многопоточные переводы, чтобы посчитать сумму в банке после всей этой анархии
    public static boolean areAllThreadsComplete(Thread[] threads) {
        boolean result = true;
        for (Thread thread : threads) {
            if (thread.isAlive()) {
                result = false;
                break;
            }
        }
        return result;
    }

}
