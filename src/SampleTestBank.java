import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SampleTestBank extends TestCase {
    Bank bank;
    Random random;
    int numOfAccount = 10;
    final int threadCount = 10;
    final int transfersPerThread = 500000;
    List<Account> acc = new ArrayList<>();

    @Override
    protected void setUp() {
        bank = new Bank(numOfAccount);
        random = new Random();
        acc.addAll(bank.getAccounts().values());
    }

    @Test
    public void testBank() throws InterruptedException {
        long sumOfMoney = bank.getAccounts().values().stream().mapToLong(Account::getMoney).sum();
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            threads.add(new Thread(() -> {
                for (int j = 0; j < transfersPerThread; j++) {
                    Account from = acc.get(random.nextInt(acc.size()));
                    Account to = acc.get(random.nextInt(acc.size()));
                    int sum = random.nextInt(50500);
                    try {
                        bank.transfer(from.getAccNumber(), to.getAccNumber(), sum);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }));
        }
        threads.forEach(Thread::start);
        for (Thread thread : threads) {
            thread.join();
        }

        long result = bank.getAccounts().values().stream().mapToLong(Account::getMoney).sum();
        System.out.println(sumOfMoney + "\n" + result);
        bank.getAccounts().values().forEach(System.out::println);

        assertEquals(sumOfMoney, result);

    }
}
