import java.text.DecimalFormat;

public class Account  {
    private final String accNumber;
    private long money;
    private boolean activeStatus;

    public Account(String accNumber, long money) {
        this.accNumber = accNumber;
        this.money = money;
        this.activeStatus = true;
    }

    public void setMoney(long money)
    {
        this.money = money;
    }

    public void setActiveStatus(boolean activeStatus)
    {
        this.activeStatus = activeStatus;
    }

    public long getMoney()
    {
        return money;
    }

    public String getAccNumber()
    {
        return accNumber;
    }

    public boolean getActiveStatus()
    {
        return activeStatus;
    }

    //Метод, печатающий состояние счета
    public String toString()
    {
        return "Счёт № " + accNumber + ", баланс: " + new DecimalFormat( "###,###,###,###.##" ).format(money) + " рублей, " + (activeStatus ? "счет активен" : "счет заблокирован");
    }
}
