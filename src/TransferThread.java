import java.util.Random;

//Класс запускающий заданное количество случайных переводов бабла в потоке
public class TransferThread implements Runnable

{
    private final Bank bank;
    private final Random random = new Random();

    public TransferThread(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void run() {
        int accountsNumber = bank.getAccountsNumber();

        for (int i = 0; i < 1000000; i++) {
            int firstAccNum = random.nextInt(accountsNumber);
            int secondAccNum;
            do {
                secondAccNum = random.nextInt(accountsNumber);
            } while (firstAccNum == secondAccNum); // Это чтоб не попытаться перевести деньги на одном счету (ну, вдруг совпадет - на малых коллекциях вполне возможно)
            long amount = (long) (Math.random() * 50050);

            String fromAccNum = bank.getAccounts().get(bank.getAccounts().keySet().toArray()[firstAccNum]).getAccNumber();
            String toAccNum = bank.getAccounts().get(bank.getAccounts().keySet().toArray()[secondAccNum]).getAccNumber();

            try {
                bank.transfer(fromAccNum, toAccNum, amount);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
